/*! \file  acceptanceTestMasterS1.c
 *
 *  \brief
 *
 *  \author jjmcd
 *  \date August 17, 2019, 10:54 AM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include <stdio.h>
#include "../include/dsPIC-EL-CH.h"
#include "../include/LCD.h"
#include "acceptanceTestMasterS1.h"

#define DELAY_TIME 100

/*! acceptanceTestMasterS1 - */

/*!
 *
 */
int main(void)
{
  initialize();

  while(1)
    {
      if ( SI1MBX3D)
        {
          _LATB4 ^= 1;
          Delay_ms(DELAY_TIME);
          _LATB6 ^= 1;
          Delay_ms(DELAY_TIME);
          _LATC7 ^= 1;
          Delay_ms(DELAY_TIME);
        }
      else
        {
          _LATB4 = SI1MBX0D;
          _LATB6 = SI1MBX1D;
          _LATC7 = SI1MBX2D;
        }
    }
  return 0;

}
