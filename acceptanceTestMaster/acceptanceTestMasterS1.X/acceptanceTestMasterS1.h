/*! \file  acceptanceTestMasterS1.h
 *
 *  \brief Constants and function prototypes for acceptanceTestMasterS1
 *
 *  \author jjmcd
 *  \date August 17, 2019, 10:55 AM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef ACCEPTANCETESTMASTERS1_H
#define	ACCEPTANCETESTMASTERS1_H

#ifdef	__cplusplus
extern "C"
{
#endif


  void initialize(void);

#ifdef	__cplusplus
}
#endif

#endif	/* ACCEPTANCETESTMASTERS1_H */

