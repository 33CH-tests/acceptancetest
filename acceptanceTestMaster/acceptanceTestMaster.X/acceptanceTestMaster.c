/*! \file  acceptanceTestMaster.c
 *
 *  \brief Exercise functions of the dsPIC-EL-CH board
 *
 * Program tests the LCD, buttons and LEDs on the dsPIC-EL-CH.  Also
 * used the deadman timer to restart the program after the final test
 * (buttons). This version tests the master core.
 *
 * Tests included
 * \li check buttons for shorts
 * \li cycle LEDs five times
 * \li test LCD
 * \li test buttons
 * \li fire the deadman timer
 *
 *
 *  \author jjmcd
 *  \date January 27, 2019, 5:07 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include <stdio.h>
#include <libpic30.h>
#include "../include/dsPIC-EL-CH.h"
#include "../include/LCD.h"
#include "acceptanceTestMaster.h"
#include "acceptanceTestMasterS1.h"


/*! main - Initial testing of dsPIC-EL-CH */

/*! Runs a number of initial tests on the dsPIC-EL-CH
 *
 */
int main(void)
{
  char szWork[32];

  /* Initialize the ports and LCD */
  initialize();

  /* Program and start the slave */
  _program_slave(1, 0, acceptanceTestMasterS1);
  _start_slave();

  MSI1MBX3D = 0xffff;
  MSI1MBX0D = 0x0000;
  MSI1MBX1D = 0x0000;
  MSI1MBX2D = 0x0000;


  /* Display a startup banner */
  LCDclear();
  LCDputs("  dsPIC-EL-CH   ");
  LCDline2();
  LCDputs("      Test      ");
  Delay_ms(2000);

  /* Do a quick check of the buttons before starting */
  checkButtons();

  MSI1MBX3D = 0;
  Delay_ms(500);

  /* Test the LEDs */
  testLED();

  Delay_ms(500);
  MSI1MBX3D = 0xffff;

  /* Exercise the LCD */
  testLCD();

  /* Do a test of the button presses */
  testButtons();

  /* Turn on the deadman timer to reset after we do this a while */
  DMTCONbits.ON = 1;
  _DMTIE = 1;   /* Enable deadman timer interrupt */

  while (1)
    {
      LCDposition(0x40);
      LCDputs("  Deadman Test   ");
      sprintf(szWork, "  %5u   %5u   ", 65535U-DMTCNTH,DMTCNTH);
      LCDhome();
      LCDputs(szWork);
      //Delay_ms(100);
    }

  return 0;
}

/*! _DMTInterrupt() - Deadman timer interrupt service routine */
/*!  Cause software reset on deadman timer interrupt
 */
void __attribute__((__interrupt__, no_auto_psv)) _DMTInterrupt(void)
{
  __builtin_software_breakpoint();
}
