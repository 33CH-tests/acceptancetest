/*! \file  testLED.c
 *
 *  \brief Cycle the LEDs
 *
 *
 *  \author jjmcd
 *  \date September 16, 2015, 8:28 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "../include/dsPIC-EL-CH.h"
#include "../include/LCD.h"
#include "acceptanceTestMaster.h"


/*! cycleLEDs() - Cycle the LEDs */

/*! The LEDs are exercised in the following sequence:
 * \li LED1 is illuminated
 * \li LED2 is illuminated
 * \li LED3 is illuminated
 * \li LED1 is extinguished
 * \li LED2 is extinguished
 * \li LED3 is extinguished
 * The LCD display shows which LEDs are illuminated.  Each condition
 * is held for one second.
 *
 */
void cycleLEDs(void)
{
  LCDposition(12);
  LCDputs("LED1");
  //LED1 = 1;
  MSI1MBX0D = 0x1;
  Delay_ms(1000);
  LCDposition(6);
  LCDputs("LED2");
  //LED2 = 1;
  MSI1MBX1D = 0x1;
  Delay_ms(1000);
  LCDposition(0);
  LCDputs("LED3");
  //LED3 = 1;
  MSI1MBX2D = 0x1;
  Delay_ms(1000);

  LCDposition(12);
  LCDputs("    ");
  //LED1 = 0;
  MSI1MBX0D = 0x0;
  Delay_ms(1000);
  //LED2 = 0;
  MSI1MBX1D = 0x0;
  LCDposition(6);
  LCDputs("    ");
  Delay_ms(1000);
  //LED3 = 0;
  MSI1MBX2D = 0x0;
  LCDposition(0);
  LCDputs("    ");
  Delay_ms(1000);

}

/*! testled() - Test the LEDs */
/*! Test the LEDs. Display a messge for a second, then call
 *  cycleLeds() three times.
 */
void testLED( void )
{
  int i;

  LCDputs("     Cycle      ");
  LCDline2();
  LCDputs("     LEDs       ");
  Delay_ms(1000);
  LCDclear();
  for (i = 0; i < 3; i++)
    cycleLEDs();

}