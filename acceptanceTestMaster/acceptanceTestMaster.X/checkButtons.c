/*! \file  checkButtons.c
 *
 *  \brief Check that all the buttons read high at the start
 *
 *
 *  \author jjmcd
 *  \date September 16, 2015, 7:30 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "../include/dsPIC-EL-CH.h"
#include "../include/LCD.h"

/*! Remember any button failures */
int nFailedButtons[4];

/*! checkButtons() - Warn if all buttons are not high */

/*! Function checks that all buttons are high (not pressed).
 *  If any buttons low, the button number(s) are displayed on the LCD
 *  and LED2 flashed rapidly 100 times.
 */
void checkButtons(void)
{
  int i;
  int total;

  /* Initialize to all buttons ok */
  for ( i=0; i<4; i++ )
    nFailedButtons[i]=0;

  /* Check that each button is up at the start */
  if ( BUTTON_UP )
    nFailedButtons[0]=1;
  if ( BUTTON_LEFT )
    nFailedButtons[1]=1;
  if ( BUTTON_RIGHT )
    nFailedButtons[2]=1;
  if ( BUTTON_DOWN )
    nFailedButtons[3]=1;

  /* Note if any buttons are marked failed */
  total = 0;
  for ( i=0; i<4; i++ )
    total = total + nFailedButtons[i];
  LCDclear();

  /* Display whether anything failed or all is good */
  if ( total )
    {
      LCDputs(" Buttons Failed ");
      for ( i=0; i<4; i++ )
        {
          if ( nFailedButtons[i] )
            {
              LCDposition(0x40+4*i);
              LCDputs("SW");
              LCDletter(0x31+i);
            }
        }
      for ( i=0; i<100; i++ )
        {
          LED2 ^= 1;
          Delay_ms(100);
        }
    }
  else
    {
      LCDclear();
      LCDputs("  Initial check ");
      LCDline2();
      LCDputs("  buttons good  ");
    }
  Delay_ms(1000);
  LCDclear();

}
