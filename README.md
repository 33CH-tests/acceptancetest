# dsPIC-EL-CH Acceptance Test

## Test of various functions of dsPIC-EL-CH

Application first reads buttons to be sure they are all un-pushed. It 
then cycles through LEDs, displaying the LED status on the LCD.  Then 
it does some LCD exercising. Next, it allows for the buttons to be
pressed and displays their state.  Finally, it watches the Deadman
counter and halts when the deadman timer expires

In acceptanceTestMaster, master controls LCD and buttons, but slave
controls LEDs. Slave cycled LEDs until LED test when slave illuminates
LEDs under instructions from master.


## Projects:

- acceptanceTestMaster - Tests for master core
  -  acceptanceTestMaster.X - Master test, master core load
  -  acceptanceTestMasterS1.X - Master test, slave core load
- acceptanceTestSlave - Tests for slave core  
  - acceptanceTestSlave.X - Slave test, master core load
  - acceptanceTestSlaveS1.X - Slave test, slave core load
