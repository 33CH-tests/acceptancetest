/*! \file  acceptanceTestSlave.c
 *
 *  \brief Exercise functions of the dsPIC-EL-CH board
 *
 * Program tests the LCD, buttons and LEDs on the dsPIC-EL-CH.  Also
 * used the deadman timer to restart the program after the final test
 * (buttons). This version tests the master core.
 *
 * Tests included
 * \li check buttons for shorts
 * \li cycle LEDs five times
 * \li test LCD
 * \li test buttons
 * \li fire the deadman timer
 *
 *
 *  \author jjmcd
 *  \date January 27, 2019, 5:07 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include <stdio.h>
#include <libpic30.h>
#include "../include/dsPIC-EL-CH.h"
#include "acceptanceTestSlave.h"
#include "acceptanceTestSlaveS1.h"

/*! main - Initial testing of dsPIC-EL-CH */

/*! Runs a number of initial tests on the dsPIC-EL-CH
 *
 */
int main(void)
{

  /* Initialize the ports and LCD */
  initialize();

  /* Program and start the slave */
  _program_slave(1, 0, acceptanceTestSlaveS1);
  _start_slave();

  while (1)
    {
    }

  return 0;
}
