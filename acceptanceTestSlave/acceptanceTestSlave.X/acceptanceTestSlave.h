/*! \file  acceptanceTestSlave.h
 *
 *  \brief Function prototypes and manifest constants
 *
 *
 *  \author jjmcd
 *  \date September 16, 2015, 5:12 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef ACCEPTANCETEST_H
#define	ACCEPTANCETEST_H

#ifdef	__cplusplus
extern "C"
{
#endif

/*! initialize() - Set up the clock and ports and initialize the LCD */
void initialize(void);

#ifdef	__cplusplus
}
#endif

#endif	/* ACCEPTANCETEST_H */

