/*! \file  testLCD.c
 *
 *  \brief Exercise the LCD functions
 *
 *
 *  \author jjmcd
 *  \date September 16, 2015, 8:13 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "../include/LCD.h"

char szLine1[18]="* ... Test ... *";
char szLine2[18]="*  LCDposition *";

/*! testLCD() - Exercise the LCD functions */

/*! Clears the LCD and displays 'TEST LCD' for one second.  All 32
 *  LCD positions are then filled with alphabetic characters and the
 *  display preserved for two seconds.  Next, a 'Test LCDposition' 
 *  display is generated one character at a time, with a delay of
 *  10 ms between each character.  The top line is written left to
 *  right while the bottom line is written right to left.  After
 *  another two second delay, an LCDshiftLeft message is displayed
 *  and the display shifted off to the left 200 ms per character.
 *  A similar operation is performed for LCDshiftRight.
 */
void testLCD(void)
{
  int i;
  
  /* Display banner to show what we are doing */
  LCDclear();
  LCDputs("    Test LCD    ");
  Delay_ms(1000);

  /* Fill the display */
  LCDclear();
  LCDhome();
  LCDputs("0123456789ABCDEF");
  LCDline2();
  LCDputs("abcdefghijklmnop");
  Delay_ms(2000);
  
  /* Display a message character by character to show LCDposition() */
  LCDclear();
  for ( i=0; i<16; i++ )
    {
      LCDposition(i);
      LCDletter(szLine1[i]);
      Delay_ms(100);
      LCDposition(0x40+15-i);
      LCDletter(szLine2[15-i]);
      Delay_ms(100);
    }
  Delay_ms(2000);
  
  /* Demonstrate shift left */
  LCDclear();
  LCDputs("      Test      ");
  LCDline2();
  LCDputs("  LCDshiftLeft  ");
  Delay_ms(500);
  for ( i=0; i<16; i++ )
    {
      LCDshiftLeft();
      Delay_ms(200);
    }
  Delay_ms(500);
  
  /* Demonstrate shift right */
  LCDclear();
  LCDputs("     Test      ");
  LCDline2();
  LCDputs(" LCDshiftRight ");
  Delay_ms(500);
  for ( i=0; i<16; i++ )
    {
      LCDshiftRight();
      Delay_ms(200);
    }
  Delay_ms(500);
}
