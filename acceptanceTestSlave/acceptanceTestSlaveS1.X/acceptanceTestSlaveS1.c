/*! \file  acceptanceTestSlaveS1.c
 *
 *  \brief Exercise functions of the dsPIC-EL-CH board
 *
 * Program tests the LCD, buttons and LEDs on the dsPIC-EL-CH.  Also
 * used the deadman timer to restart the program after the final test
 * (buttons). This version tests the slave core.
 *
 * Tests included
 * \li check buttons for shorts
 * \li cycle LEDs five times
 * \li test LCD
 * \li test buttons
 * \li fire the deadman timer
 *
 *
 *  \author jjmcd
 *  \date January 27, 2019, 5:07 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include <stdio.h>
#include "../include/dsPIC-EL-CH.h"
#include "../include/LCD.h"
#include "acceptanceTestSlaveS1.h"

/*! main - Initial testing of dsPIC-EL-CH */

/*! Runs a number of initial tests on the dsPIC-EL-CH
 *
 */
int main(void)
{
  int i;
  char szWork[32];

  /* Initialize the ports and LCD */
  initialize();

  /* Display a startup banner */
  LCDclear();
  LCDputs("  dsPIC-EL-CH   ");
  LCDline2();
  LCDputs("      Test      ");
  Delay_ms(2000);

  /* Do a quick check of the buttons before starting */
  checkButtons();
  LCDclear();

  LCDputs("     Cycle      ");
  LCDline2();
  LCDputs("     LEDs       ");
  Delay_ms(1000);
  LCDclear();
  for (i = 0; i < 5; i++)
    testLED();

  /* Exercise the LCD */
  testLCD();

  /* Do a test of the button presses */
  testButtons();
  LCDline2();
  LCDputs("    D O N E     ");
  Delay_ms(1000);

  /* Turn on the deadman timer to reset after we do this a while */
  DMTCONbits.ON = 1;
  _DMTIE = 1;

  LCDclear();
  LCDputs("When DMT expires");
  LCDline2();
  LCDputs("ISR is triggered");
  Delay_ms(1000);

  LCDclear();
  LCDputs("    Turns on    ");
  LCDline2();
  LCDputs(" LED1 and LED3  ");
  Delay_ms(1000);
  LCDclear();

  while (1)
    {
      LCDposition(0x40);
      LCDputs("  Deadman Test   ");
      sprintf(szWork, "     %5u     ", DMTCNTH);
      LCDhome();
      LCDputs(szWork);
      //Delay_ms(100);
    }

  return 0;
}

void __attribute__((__interrupt__, auto_psv)) _DMTInterrupt( void )
{
  _DMTIF = 0;
  DMTCONbits.ON = 0;
  _LATC7 = 1;
  _LATB4 = 1;
//__builtin_software_breakpoint();
}
