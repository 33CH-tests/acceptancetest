/*! \file  acceptanceTest-slaveS1.h
 *
 *  \brief Constants and function prototypes for acceptanceTest-slaveS1
 *
 *  \author jjmcd
 *  \date January 28, 2019, 9:47 AM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef ACCEPTANCETEST_SLAVES1_H
#define	ACCEPTANCETEST_SLAVES1_H

#ifdef	__cplusplus
extern "C"
{
#endif



/*! initialize() - Set up the clock and ports and initialize the LCD */
void initialize(void);
/*! testButtons() - Check that button presses can be detected */
void testButtons(void);
/*! checkButtons() - Warn if all buttons are not high */
void checkButtons(void);
/*! testLCD() - Exercise the LCD functions */
void testLCD(void);
/*! testLED() - Cycle the LEDs */
void testLED(void);


#ifdef	__cplusplus
}
#endif

#endif	/* ACCEPTANCETEST_SLAVES1_H */

